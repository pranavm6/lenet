#include <iostream>
#include <cmath>
#include <algorithm>
#include <fstream>
#include <vector>
#include <array>
#include <sys/time.h>
#include <omp.h>
#include "mpi.h"

#define NUM_TESTS 10000

using namespace std;

//Static allocation of test images
unsigned char images[NUM_TESTS*28*28];
unsigned char labels[NUM_TESTS];
// *****************************************

//Static allocation of network parameters and their outputs
float image[1][32][32] = {0};
float conv1_weights[6][1][5][5] = {0};
float conv1_bias[6] = {0};
float conv1_output[6][28][28] = {0};

float pool2_output[6][14][14] = {0};

float conv3_weights[16][6][5][5] = {0};
float conv3_bias[16] = {0};
float conv3_output[16][10][10] = {0};

float pool4_output[16][5][5] = {0};

float conv5_weights[120][16][5][5] = {0};
float conv5_bias[120] = {0};
float conv5_output[120][1][1] = {0};

float fc6_weights[10][120][1][1] = {0};
float fc6_bias[10] = {0};
float fc6_output[10] = {0};
// *****************************************
// End declaration of layers parameters and buffers
// *****************************************
int kRank, kNumPes;

// Start function definitions of different layers
inline float relu(float input) {
    return (input > 0)? input:0;
}

// Convolution Layer 1
void convolution1(float input[1][32][32], float weights[6][1][5][5], float bias[6], float output[6][28][28]) {
    float sum;
    float out[1][28][28];
    float output_coll[8][28][28];

    if(kRank < 6) {
        for(int h = 0; h < 28; h++) {
            for(int w = 0; w < 28; w++) {
                for(int i = 0; i < 5; i++) {
                    for(int j = 0; j < 5; j++) {
                        if((i == 0) && (j == 0)) sum = 0;
                        sum += weights[kRank][0][i][j] * input[0][i+h][j+w];
                        if((i == 4) && (j == 4)) {
                            out[0][h][w] = (sum + bias[kRank] > 0) ? sum + bias[kRank] : 0;
                            //if(kRank == 1) printf("%lf ",sum);
                        }
                    }
                }
            }
        }
    }
    MPI_Allgather(&out,28*28,MPI_FLOAT,&output_coll,28*28,MPI_FLOAT,MPI_COMM_WORLD);

    for(int k=0; k<6; k++) {
        for(int b=0; b<28; b++) {
            for(int c=0; c<28; c++) {
                output[k][b][c] = output_coll[k][b][c];
            }
        }
    }
}

// Relu Layer 1
void relu1(float input[6][28][28], float output[6][28][28]) {
    for(int i = 0; i < 6; i++) {
        for(int j = 0; j < 28; j++) {
            for(int k = 0; k < 28; k++) {
                output[i][j][k] = relu(input[i][j][k]);
            }
        }
    }
}

// Pooling Layer 2
void max_pooling2(float input[6][28][28],float output[6][14][14]) {
    float out[1][14][14];
    float output_coll[8][14][14];

    //if(kRank == 1) {
    //    for(int b=0; b<28; b++) {
    //        for(int c=0; c<28; c++) {
    //            printf("%lf ",input[1][b][c]);
    //        }
    //    }
    //}

    if(kRank < 6) {
        for(int h = 0; h < 14; h++) {
            for(int w = 0; w < 14; w++) {
                float max_value=-1000000000000.0;
                for(int i = h*2; i < h*2+2; i++) {
                    for(int j = w*2;j < w*2+2; j++) {
                        max_value = (max_value > input[kRank][i][j]) ? max_value:input[kRank][i][j];
                    }
                }                
                out[0][h][w] = max_value;
                //if(kRank == 1) printf("%lf ",out[0][h][w]);
            }
        }
    }
    MPI_Allgather(&out,14*14,MPI_FLOAT,&output_coll,14*14,MPI_FLOAT,MPI_COMM_WORLD);

    for(int k=0; k<6; k++) {
        for(int b=0; b<14; b++) {
            for(int c=0; c<14; c++) {
                output[k][b][c] = output_coll[k][b][c];
            }
        }
    }
}

// Relu Layer 2
void relu2(float input[6][14][14], float output[6][14][14]) {
    for(int i = 0; i < 6; i++) {
        for(int j = 0; j < 14; j++) {
            for(int k = 0; k < 14; k++) {
                output[i][j][k] = relu(input[i][j][k]);
            }
        }
    }
}

// Convolution Layer 3
void convolution3(float input[6][14][14], float weights[16][6][5][5], float bias[16], float output[16][10][10]) {
    float sum = 0;
    float out[2][10][10];
    float output_coll[16][10][10];

    if(kRank < 8) {
        for(int co = 0; co < 2; co++) {
            for(int h = 0; h < 10; h++) {
                for(int w = 0; w < 10; w++) {
                    for(int i = 0; i < 5; i++) {
                        for(int j = 0; j < 5; j++) {
                            for (int ci = 0; ci < 6; ci++) {
                                if((ci == 0) && (i == 0) && (j == 0)) sum = 0;
                                sum += weights[2*kRank+co][ci][i][j] * input[ci][i+h][j+w];
                                if((ci == 5) && (i == 4) && (j==4)) out[co][h][w] = (sum + bias[2*kRank+co] > 0) ? sum + bias[2*kRank+co] : 0;
                            }
                        }
                    }
                }
            }
        }
    }
    MPI_Allgather(&out,2*10*10,MPI_FLOAT,&output_coll,2*10*10,MPI_FLOAT,MPI_COMM_WORLD);

    for(int k=0; k<16; k++) {
        for(int b=0; b<10; b++) {
            for(int c=0; c<10; c++) {
                output[k][b][c] = output_coll[k][b][c];
            }
        }
    }
}

// Relu Layer 3
void relu3(float input[16][10][10], float output[16][10][10])
{
    for(int i = 0; i < 16; i++) {
        for(int j = 0; j < 10; j++) {
            for(int k = 0; k < 10; k++) {
                output[i][j][k] = relu(input[i][j][k]);
            }
        }
    }
}

// Pooling Layer 4
void max_pooling4(float input[16][10][10],float output[16][5][5])
{
    float out[2][5][5];
    float output_coll[16][5][5];

    if(kRank < 8) {
        for(int c = 0;c < 2; c++) {
            for(int h = 0; h < 5; h++) {
                for(int w = 0; w < 5; w++) {
                    float max_value=-1000000000000.0;
                    for(int i = h*2; i < h*2+2; i++) {
                        for(int j = w*2;j < w*2+2; j++) {
                            max_value = (max_value > input[2*kRank+c][i][j]) ? max_value:input[2*kRank+c][i][j];
                        }
                    }
                    out[c][h][w] = max_value;
                }
            }
        }
    }
    MPI_Allgather(&out,2*5*5,MPI_FLOAT,&output_coll,2*5*5,MPI_FLOAT,MPI_COMM_WORLD);

    for(int k=0; k<16; k++) {
        for(int b=0; b<5; b++) {
            for(int c=0; c<5; c++) {
                output[k][b][c] = output_coll[k][b][c];
            }
        }
    }
}

// Relu Layer 4
void relu4(float input[16][5][5], float output[16][5][5])
{
    for(int i = 0; i < 16; i++) {
        for(int j = 0; j < 5; j++) {
            for(int k = 0; k < 5; k++) {
                output[i][j][k] = relu(input[i][j][k]);
            }
        }
    }
}

// Convolution Layer 5
void convolution5(float input[16][5][5], float weights[120][16][5][5], float bias[120], float output[120][1][1])
{
    float sum = 0;
    for(int co = 0; co < 120; co++) {
        for(int i = 0; i < 5; i++) {
            for(int j = 0; j < 5; j++) {
                for (int ci = 0; ci < 16; ci++) {
                    if((ci == 0) && (i == 0) && (j == 0)) sum = 0;
                    sum += weights[co][ci][i][j] * input[ci][i][j];
                    if((ci == 15) && (i == 4) && (j == 4)) output[co][0][0] = (sum + bias[co] > 0) ? sum + bias[co] : 0;
                }
            }
        }
    }
}

// Relu Layer 5
void relu5(float input[120][1][1], float output[120][1][1])
{
//#pragma omp parallel for schedule(static)
    for(int i = 0; i < 120; i++) {
        output[i][0][0] = relu(input[i][0][0]);
    }
}

// Fully connected Layer 6
void fc6(const float input[120][1][1], const float weights[10][120][1][1], const float bias[10], float output[10])
{
    for(int n = 0; n < 10; n++) {
        float sum = 0;
        for(int c = 0; c < 120; c++) {
            sum += weights[n][c][0][0] * input[c][0][0];
        }
        output[n] = sum + bias[n];
    }
}

// Relu Layer 6
void relu6(float input[10], float output[10])
{
//#pragma omp parallel for schedule(static)
    for(int i = 0; i < 10; i++) {
        output[i] = relu(input[i]);
    }
}
// *****************************************
// End declaration of layers functions
// *****************************************

// Parse MNIST test images
int parse_mnist_images(unsigned char *images)
{
    unsigned int header[4];

    string filename = "images.bin";
    ifstream myFile;
    myFile.open(filename.c_str());

    myFile.seekg(0, ios::beg);
    if (!myFile) {
        printf("Error seeking at the start");
    }

    myFile.read((char*)header, sizeof(unsigned int)*4);
    if (!myFile) {
        printf("Error reading image header");
    }

    myFile.read((char*)images, sizeof(unsigned char)*NUM_TESTS*28*28);
    if (!myFile) {
        printf("Error reading images");
    }

    myFile.close();
    if (!myFile) {
        printf("Error closing");
    }

    return 0;

}

// Parse MNIST test image labels
int parse_mnist_labels(unsigned char *labels)
{
    unsigned int header[2];

    string filename = "labels.bin";
    ifstream myFile;
    myFile.open(filename.c_str());

    myFile.seekg(0, ios::beg);
    if (!myFile) {
        printf("Error seeking at the start");
    }

    myFile.read((char*)header, sizeof(unsigned int)*2);
    if (!myFile) {
        printf("Error reading labels header");
    }

    myFile.read((char*)labels, sizeof(unsigned char)*NUM_TESTS);
    if (!myFile) {
        printf("Error reading images");
    }

    myFile.close();
    if (!myFile) {
        printf("Error closing");
    }

    return 0;
}

// Parse parameter file and load it in to the arrays
int parse_parameters()
{
    string filename = "params.bin";
    ifstream myFile;
    myFile.open(filename.c_str());

    myFile.seekg(0, ios::beg);
    if (!myFile) {
        printf("Error seeking at the start");
    }

    myFile.read((char*)***conv1_weights, sizeof(float)*150);
    if (!myFile) {
        printf("Error reading conv1 weights");
    }

    myFile.read((char*)conv1_bias, sizeof(float)*6);
    if (!myFile) {
        printf("Error reading conv1 bias");
    }

    myFile.read((char*)***conv3_weights, sizeof(float)*2400);
    if (!myFile) {
        printf("Error reading conv3 weights");
    }

    myFile.read((char*)conv3_bias, sizeof(float)*16);
    if (!myFile) {
        printf("Error reading conv3 bias");
    }

    myFile.read((char*)***conv5_weights, sizeof(float)*48000);
    if (!myFile) {
        printf("Error reading conv5 weights");
    }

    myFile.read((char*)conv5_bias, sizeof(float)*120);
    if (!myFile) {
        printf("Error reading conv5 bias");
    }

    myFile.read((char*)***fc6_weights, sizeof(float)*1200);
    if (!myFile) {
        printf("Error reading fc6 weights");
    }

    myFile.read((char*)fc6_bias, sizeof(float)*10);
    if (!myFile) {
        printf("Error reading fc6 bias");
    }

    myFile.close();
    if (!myFile) {
        printf("Error closing");
    }

    return 0;

}

// Fetch a single image to be processed.
//
void get_image(unsigned char *images, unsigned int idx, float image[1][32][32])
{
    for(int i = 0; i < 32; i++)
        for(int j = 0; j < 32; j++)
        {
            if (i < 2 || i > 29 || j < 2 || j > 29)
                image[0][i][j] = -1.0;
            else
                image[0][i][j] = images[idx*28*28 + (i-2)*28 + j-2] / 255.0 * 2.0 - 1.0;
        }
}

long long get_time() {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec * 1000000 + tv.tv_usec;
}

long long stop_timer(long long start_time) {
    struct timeval tv;
    gettimeofday(&tv, NULL);
    long long end_time = tv.tv_sec * 1000000 + tv.tv_usec;
    double elapsed = (end_time - start_time);
    printf(" %.9f usec\n", elapsed);
    return end_time - start_time;
}

void setup_env(int argc, char *argv[]) {
  // Initialize the MPI environment
  MPI_Init(&argc, &argv);
  // Get the rank of the process
  MPI_Comm_rank(MPI_COMM_WORLD, &kRank);
  // Get the number of processes
  MPI_Comm_size(MPI_COMM_WORLD, &kNumPes);
}

int main(int argc, char **argv)
{
    setup_env(argc,argv);

    int num_correct = 0;
    //printf("Parsing MNIST images\n\r");
    parse_mnist_images(images);
    
    //printf("Parsing MNIST labels\n\r");
    parse_mnist_labels(labels);
    
    //printf("Parsing parameters\n\r");
    parse_parameters();
    
    //printf("Starting inference\n\r");

    struct timeval tv1;
    long long start_time;
    if(kRank == 0) {
        gettimeofday(&tv1, NULL);
        start_time = tv1.tv_sec * 1000000 + tv1.tv_usec;;
        //printf("Start time: %lld us\n",start_time);
    }

    //printf("\n\rTest Image: ");
    for (int k = 0; k < NUM_TESTS; k++)
    {
    	//Get test image from dataset
        get_image(images, k, image);
        MPI_Barrier(MPI_COMM_WORLD);

        convolution1(image, conv1_weights, conv1_bias, conv1_output);
        MPI_Barrier(MPI_COMM_WORLD);

        max_pooling2(conv1_output, pool2_output);
        MPI_Barrier(MPI_COMM_WORLD);

        convolution3(pool2_output, conv3_weights, conv3_bias, conv3_output);
        MPI_Barrier(MPI_COMM_WORLD);
        
        max_pooling4(conv3_output, pool4_output);
        MPI_Barrier(MPI_COMM_WORLD);
        
        if(kRank == 0) {
            convolution5(pool4_output, conv5_weights, conv5_bias, conv5_output);
            
            fc6(conv5_output, fc6_weights, fc6_bias, fc6_output);
            
            //Check which output was largest.
            unsigned char result = 0;
            float p = -1000000.0;
            for(int i = 0; i < 10; i++)
            {
                if(fc6_output[i] > p)
                {
                    p = fc6_output[i];
                    result = i;
                }
            }
            //Largest output is result

            //printf("test %0d: %0d / %0d : ",k,int(result),int(labels[k]));
            if(result == labels[k])
            {
                num_correct++;
                //printf("correct\n");
            }
            else
            {
                //printf("WRONG\n");
            }

            //printf("\n\rTest Image: ");
        }
    }

    struct timeval tv2;
    long long end_time;
    long long total_time;
    if(kRank == 0) {
        gettimeofday(&tv2, NULL);
        end_time = tv2.tv_sec * 1000000 + tv2.tv_usec;
        total_time = end_time - start_time; 
        printf("End time: %lld us\n",end_time);
        printf("Total time: %lld usecs\n",total_time);

        printf("\n\rAccuracy = %lf %\n",(float)(num_correct)/(float)NUM_TESTS * 100.0);
    }

    return 0;
}

